% ADAPTIVITY_ESTIMATE_L2PROJ: Computation of a posteriori error indicators for the L2-proj, using smooth (C^1) hierarchical spaces.
%
%
% USAGE:
%
% est = adaptivity_estimate_l2proj (u, hmsh, hspace, problem_data, adaptivity_data)
%
% INPUT:
%
%   u:      degrees of freedom
%   hmsh:   object representing the hierarchical mesh (see hierarchical_mesh)
%   hspace: object representing the space of hierarchical splines (see hierarchical_space)
%   problem_data: a structure with data of the problem. For this function, it must contain the fields:
%    - f:            source term
%   adaptivity_data: a structure with the data for the adaptivity method. In particular, it contains the fields:
%    - flag:         'elements' or 'functions', depending on the refinement strategy.
%
%
% OUTPUT:
%
%   est: computed a posteriori error indicators
%           - When adaptivity_data.flag == 'elements': for an element Q,
%                          est_Q := (int_Q |f - U|^2)^(1/2),
%           where U is the L2-projection of f
%           - When adaptivity_data.flag == 'functions': for a B-spline basis function b,
%                          est_b := (int_{supp b} a_b*|f - U|^2*b)^(1/2),
%           where a_b is the coefficient of b for the partition-of-unity in the hierarchical basis, and U is the L2-projection of f
%
%

function est = adaptivity_estimate_l2proj (u, hmsh, hspace, uex, flag)

[valU, F] = hspace_eval_hmsh (u, hspace, hmsh, 'value');

x = cell (hmsh.rdim, 1);
for idim = 1:hmsh.rdim;
    x{idim} = reshape (F(idim,:), [], hmsh.nel);
end

valf = uex (x{:});

aux = (valf - valU).^2; % size(aux) = [hmsh.nqn, hmsh.nel], interior residual at quadrature nodes

switch flag
    case 'elements',
        quad_weights = [];
        jacdet = [];
        for ilev = 1:hmsh.nlevels % Active levels
            if (hmsh.msh_lev{ilev}.nel ~= 0)
                quad_weights = cat(2,quad_weights, hmsh.msh_lev{ilev}.quad_weights);
                jacdet = cat(2,jacdet, hmsh.msh_lev{ilev}.jacdet);
            end
        end
        w = quad_weights .* jacdet;
        est = sqrt (sum (aux.*w));
    case 'functions',
        % XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        % If we allow that the third input of op_f_v_hier can be f at the
        % quadrature nodes, we can use the following line instead of the
        % lines below.
        % est = sqrt(hspace.coeff_pou(:)*op_f_v_hier (hspace, hmsh, aux));
        
        est = zeros(hspace.ndof,1);
        ndofs = 0;
        Ne = cumsum([0; hmsh.nel_per_level(:)]);
        for ilev = 1:hmsh.nlevels
            ndofs = ndofs + hspace.ndof_per_level(ilev);
            if (hmsh.nel_per_level(ilev) > 0)
                ind_e = (Ne(ilev)+1):Ne(ilev+1);
                sp_lev = sp_evaluate_element_list (hspace.space_of_level(ilev), hmsh.msh_lev{ilev}, 'value', true);
                sp_lev = change_connectivity_localized_Csub (sp_lev, hspace, ilev);
                b_lev = op_f_v (sp_lev, hmsh.msh_lev{ilev}, aux(:,ind_e));
                dofs = 1:ndofs;
                est(dofs) = est(dofs) + hspace.Csub{ilev}'*b_lev;
            end
        end
        est = sqrt(hspace.coeff_pou(:).*est);
end
