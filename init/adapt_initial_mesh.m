function [U, hmsh, hspace, U_norm, info] = adapt_initial_mesh(u, hmsh, hspace, geometry, tol, adaptivity_data)
% 
% function [U, hmsh, hspace] = adapt_initial_mesh(u, hmsh, hspace, tol)
%
% L2-projection onto hierarchical spaces for d-dimensional domains (d = 1,2,3), using
% adaptive refinement
%
% This function adaptively refines the hierarchical mesh hmsh in order to get a discrete approximation U of a given function u such that \| u - U \|_{L^2(\Omega) < tol.
%
% u: function handle
% hmsh
% tol > 0
%
% U: degrees of freedom 
% hmsh: hierarchical mesh where U is defined.
%


plot_mesh = 0;
plot_discrete_solution = 0;
pausas = 0;

iter_data = [];
ndof_data = [];
nel_data = [];
error_data = [];
est_data = [];

iter = 0;

while 1
    
    iter = iter + 1;
    
    fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Iteration %d %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n',iter);
    
%     if ~hspace_check_partition_of_unity(hspace, hmsh)
%         disp('ERROR: The partition-of-the-unity property does not hold.')
%         return,
%     end
    
    if (plot_mesh)
        hmsh_plot_cells (hmsh, 10, 1);
        view(2),
        axis off
        axis square
        title(sprintf('Iteration %d - Number of elements = %d - DOFs = %d', iter, hmsh.nel, hspace.ndof))
        %        plot_hmesh_param(hmsh, 1); % In figure(1)
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% SOLVE
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    U = adaptivity_solve_l2proj (hmsh, hspace, u);
    
    if plot_discrete_solution
        %figure(2)
        npts = 51 * ones (1, hmsh.ndim);
        figura = 2;
        plot_numerical_and_exact_solution (U, hspace, geometry, npts, u, figura);
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Error computation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [errl2, errl2_elem] = sp_l2_error (hspace, hmsh, U, u);
    fprintf('error_L2 = %g\n', errl2);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% ESTIMATE
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tic
    disp('Computing error estimators:')
    est = adaptivity_estimate_l2proj(U, hmsh, hspace, u, adaptivity_data.flag);
    gest = norm(est);
    tempo = toc;
    fprintf('EST: %f (%f seconds)\n', gest, tempo);
    
    if abs(errl2-gest) > 1e-10
        disp('Warning: something is wrong in the computation of the estimators.')
        return;
    end
    
    iter_data = [iter_data iter];
    ndof_data = [ndof_data hspace.ndof];
    nel_data = [nel_data hmsh.nel];
    error_data = [error_data errl2];
    est_data = [est_data gest];
    
    if gest < tol
        disp('Success: The error estimation reached the desired tolerance')
        break,
    end
    
    if iter == adaptivity_data.num_max_iter
        disp('Warning: Maximum amount of iterations reached')
        break;
    end
    
    if hmsh.nlevels >= adaptivity_data.max_level
        disp('Warning: Maximum amount of levels reached')
        break;
    end
    
    if hspace.ndof > adaptivity_data.max_ndof
        disp('Warning: Maximum allowed DOFs achieved')
        break;
    end
    
    if hmsh.nel > adaptivity_data.max_nel
        disp('Warning: Maximum allowed amount of elements achieved')
        break;
    end
    
    if pausas
        pause
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% MARK
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    tic
    disp('Marking:')
    [marked, num_marked] = adaptivity_mark(est, hmsh, hspace, adaptivity_data);
    tempo = toc;
    %nest = numel(est);
    switch adaptivity_data.flag
        case 'elements',
            fprintf('%d elements marked for refinement (%f seconds)\n', num_marked, tempo);
        case 'functions',
            fprintf('%d functions marked for refinement (%f seconds)\n', num_marked, tempo);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% REFINE
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [hmsh, hspace] = adaptivity_refine (hmsh, hspace, marked, adaptivity_data);
    
    fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
   
end

% Compute the energy norm of the initial data
%  I am neglecting here the heat conductivity and the reaction term
[~,~,U_norm] = sp_h1_error (hspace, hmsh, U, @(x,y) zeros(size(x)), @(x,y) zeros([hmsh.rdim, size(x)]));


info.iter_data = iter_data;
info.ndof_data = ndof_data;
info.nel_data = nel_data;
info.error_data = error_data;
info.est_data = est_data;