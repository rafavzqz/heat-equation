% ADAPTIVITY_SOLVE_L2PROJ: assemble and solve the linear system for L2-projection, using hierarchical spaces.
%
% The function solves the problem
%
%     int_Omega u v dx = int_Omega f v dx, for v in the hierarchical space,
%     and f given.
%
% USAGE:
%
% u = adaptivity_solve_l2proj (hmsh, hspace, problem_data)
%
% INPUT:
%
%   hmsh:   object representing the hierarchical mesh (see hierarchical_mesh)
%   hspace: object representing the space of hierarchical splines (see hierarchical_space)
%   problem_data: a structure with data of the problem. For this function, it must contain the field:
%    - f:            source term
%
% OUTPUT:
%
%   u: computed degrees of freedom
%
% WARNING: Por el momento, solo para condiciones Dirichlet homogeneas en el
% borde
%

function u = adaptivity_solve_l2proj (hmsh, hspace, f)

% stiff_mat = op_gradu_gradv_hier (hspace, hspace, hmsh, problem_data.c_diff);
rhs = op_f_v_hier (hspace, hmsh, f);

% switch nargin(f)
%     case 1
%         h = @(x, ind) f(x);
%         drchlt_sides = [1 2];
%     case 2
%         h = @(x, y, ind) f(x,y);
%         drchlt_sides = [1 2 3 4];
%     case 3
%         h = @(x, y, z, ind) f(x,y,z);
%         drchlt_sides = [1 2 3 4 5 6];
% end

mass = op_u_v_hier (hspace, hspace, hmsh);

% % Apply Neumann boundary conditions
% if (~isfield (struct (hmsh), 'npatch')) % Single patch case
%   for iside = problem_data.nmnn_sides
%     if (hmsh.ndim > 1)
% % Restrict the function handle to the specified side, in any dimension, gside = @(x,y) g(x,y,iside)
%       gside = @(varargin) problem_data.g(varargin{:},iside);
%       dofs = hspace.boundary(iside).dofs;
%       rhs(dofs) = rhs(dofs) + op_f_v_hier (hspace.boundary(iside), hmsh.boundary(iside), gside);
%     else
%       if (iside == 1)
%         x = hmsh.mesh_of_level(1).breaks{1}(1);
%       else
%         x = hmsh.mesh_of_level(1).breaks{1}(end);
%       end
%       sp_side = hspace.boundary(iside);
%       rhs(sp_side.dofs) = rhs(sp_side.dofs) + problem_data.g(x,iside);
%     end
%   end
% else % Multipatch case
%   boundaries = hmsh.mesh_of_level(1).boundaries;
%   Nbnd = cumsum ([0, boundaries.nsides]);
%   for iref = problem_data.nmnn_sides
%     iref_patch_list = Nbnd(iref)+1:Nbnd(iref+1);
%     gref = @(varargin) problem_data.g(varargin{:},iref);
%     rhs_nmnn = op_f_v_hier (hspace.boundary, hmsh.boundary, gref, iref_patch_list);
%     rhs(hspace.boundary.dofs) = rhs(hspace.boundary.dofs) + rhs_nmnn;
%   end
% end
% 

if 1
    u = mass \ rhs;
else
    % Apply Dirichlet boundary conditions
    u = zeros (hspace.ndof, 1);
    [u_dirichlet, dirichlet_dofs] = sp_drchlt_l2_proj (hspace, hmsh, h, drchlt_sides);
    u(dirichlet_dofs) = u_dirichlet;
    
    int_dofs = setdiff (1:hspace.ndof, dirichlet_dofs);
    rhs(int_dofs) = rhs(int_dofs) - mass(int_dofs, dirichlet_dofs)*u(dirichlet_dofs);
    
    % Solve the linear system
    u(int_dofs) = mass(int_dofs, int_dofs) \ rhs(int_dofs);
end
