function [tol,partition_points, f_norm] = tolfind2(f,T_init,T,TOL, hmsh)

w = [];
quad_points = [];
for ilev = 1:hmsh.nlevels
    if (hmsh.msh_lev{ilev}.nel ~= 0)
        w = cat (2, w, hmsh.msh_lev{ilev}.quad_weights .* hmsh.msh_lev{ilev}.jacdet);
        quad_points = cat (3, quad_points, hmsh.msh_lev{ilev}.geo_map);
    end
end

MAX_INTERVALS = 1e3;
TOL2 = TOL^2;

[error2,~,norm_f2] = compute_fbar_and_error(f,[T_init T], quad_points, w);
partition_points = zeros(1,MAX_INTERVALS+1);
errors2 = zeros(1,MAX_INTERVALS);

partition_points(1:2) = [T_init T];
errors2(1) = error2;
norm_f2(1) = norm_f2;
M = 1; % # intervals

while (error2 > TOL2/2)
    M = M+1;
    [m,i] = max(errors2);
    error2 = error2 - m;
    partition_points(i+2:M+1) = partition_points(i+1:M);
    errors2(i+2:M) = errors2(i+1:M-1);
    norm_f2(i+2:M) = norm_f2(i+1:M-1);
    partition_points(i+1) = (partition_points(i)+partition_points(i+1))/2;
    [errors2(i),~,norm_f2(i)]   = compute_fbar_and_error(f,partition_points(i:i+1), quad_points, w);
    [errors2(i+1),~,norm_f2(i+1)] = compute_fbar_and_error(f,partition_points(i+1:i+2), quad_points, w);
    error2 = error2 + errors2(i) + errors2(i+1);
%    [partition_points(1:M)' errors2(1:M)']
%    error2
%    pause
end

tol = sqrt(TOL2/2/max(1,M-1));
partition_points(M+2:end)=[];

f_norm = sqrt(sum(norm_f2));
end
