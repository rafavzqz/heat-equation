%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PHYSICAL DATA OF THE PROBLEM 
% Physical domain, defined as NURBS map given in a text file
problem_data.geo_name = 'geo_square.txt';

% Type of boundary conditions for each side of the domain
problem_data.nmnn_sides   = [];
problem_data.drchlt_sides = [1 2 3 4];

% Physical parameters
problem_data.c_diff  = @(x, y) ones(size(x));
        
problem_data.c_reaction  = @(x, y) zeros(size(x)); 
        
% Final time
problem_data.T = 1;

C = 1;

% Source and boundary terms
problem_data.f = @(t, x, y) 8*pi^2*t^2*sin(2*pi*x*t).*sin(2*pi*y*t) + ...
  2*pi*(x.*cos(2*pi*x*t).*sin(2*pi*y*t) + y.*sin(2*pi*x*t).*cos(2*pi*y*t));
problem_data.g = @(t, x, y, ind) zeros(size(x));
problem_data.h = @(t, x, y, ind) sin(2*pi*x*t) .* sin(2*pi*y*t);

% Exact solution (optional)
problem_data.uex     = @(t, x, y) sin(2*pi*x*t) .* sin(2*pi*y*t);
problem_data.graduex = @(t, x, y) cat (1, ...
                       reshape (2*pi*t*cos(2*pi*x*t).*sin(2*pi*y*t), [1, size(x)]), ...
                       reshape (2*pi*t*sin(2*pi*x*t).*cos(2*pi*y*t), [1, size(x)]));
                   
problem_data.u0 = @(x,y) problem_data.uex(0,x,y);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


