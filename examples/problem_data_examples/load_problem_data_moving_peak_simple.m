%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PHYSICAL DATA OF THE PROBLEM 
% Physical domain, defined as NURBS map given in a text file
problem_data.geo_name = 'geo_big_square.txt';
problem_data.geo_name = nrb4surf ([-1 -1], [1 -1], [-1 1], [1 1]);

% Type of boundary conditions for each side of the domain
problem_data.nmnn_sides   = [];
problem_data.drchlt_sides = [1 2 3 4];

% Physical parameters
problem_data.c_diff  = @(x, y) ones(size(x));
problem_data.grad_c_diff = @(x, y) cat (1, ...
            reshape (zeros(size(x)), [1, size(x)]), ...
            reshape (zeros(size(x)), [1, size(x)]));
        
problem_data.c_reaction  = @(x, y) zeros(size(x)); 
        
% Final time
problem_data.T = 1;
        
beta = 50;
gamma = 0;
zeta = 0;

alpha = @(t) 1-zeta*exp(-gamma*(t-.5).^2);
deralpha = @(t) zeta*2*gamma*(t-.5)*exp(-gamma*(t-.5)^2);

uex = @(t,x,y) alpha(t)*exp(-beta*((x-t+.5).^2+(y-t+.5).^2));
ut = @(t,x,y) deralpha(t)*exp(-beta*((x-t+.5).^2+(y-t+.5).^2))...
    +2*beta*(x+y-2*t+1).*uex(t,x,y);

% Source and boundary terms
problem_data.f = @(t, x, y) ut(t,x,y)...
    +4*beta*(1-beta*((x-t+.5).^2+(y-t+.5).^2)).*uex(t,x,y);
problem_data.g = @(t, x, y, ind) zeros(size(x));
problem_data.h = @(t, x, y, ind) uex(t,x,y);

% Exact solution (optional)
problem_data.uex     = uex;
problem_data.graduex = @(t, x, y) cat (1, ...
                       reshape (-2*beta*(x-t+.5).*uex(t,x,y), [1, size(x)]), ...
                       reshape (-2*beta*(y-t+.5).*uex(t,x,y), [1, size(x)]));
                   
problem_data.u0 = @(x,y) problem_data.uex(0,x,y);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


