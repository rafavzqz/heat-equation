%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PHYSICAL DATA OF THE PROBLEM 
% Physical domain, defined as NURBS map given in a text file
problem_data.geo_name = 'geo_square.txt';

% Type of boundary conditions for each side of the domain
problem_data.nmnn_sides   = [];
problem_data.drchlt_sides = [1 2 3 4];

% Physical parameters
problem_data.c_diff  = @(x, y) ones(size(x));
problem_data.grad_c_diff = @(x, y) cat (1, ...
            reshape (zeros(size(x)), [1, size(x)]), ...
            reshape (zeros(size(x)), [1, size(x)]));
        
problem_data.c_reaction  = @(x, y) zeros(size(x)); 
        
% Final time
problem_data.T = 1;
        
C = 1;

% Source and boundary terms
problem_data.f = @(t, x, y) C*(2*pi*pi-1)*sin(pi*x).*sin(pi*y).*exp(-t);
problem_data.g = @(t, x, y, ind) zeros(size(x));
problem_data.h = @(t, x, y, ind) zeros(size(x));

% Exact solution (optional)
problem_data.uex     = @(t, x, y) C*sin(pi*x).*sin(pi*y).*exp(-t);
problem_data.graduex = @(t, x, y) cat (1, ...
                       reshape (C*pi*cos(pi*x).*sin(pi*y).*exp(-t), [1, size(x)]), ...
                       reshape (C*pi*sin(pi*x).*cos(pi*y).*exp(-t), [1, size(x)]));
                   
problem_data.u0 = @(x,y) problem_data.uex(0,x,y);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


