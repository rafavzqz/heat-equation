%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PHYSICAL DATA OF THE PROBLEM 
% Physical domain, defined as NURBS map given in a text file
problem_data.geo_name = 'geo_quarter_circle_epsilon.txt'; 

% Type of boundary conditions for each side of the domain
problem_data.nmnn_sides   = [];
problem_data.drchlt_sides = [1 2 3 4];

% Physical parameters
problem_data.c_diff  = @(x, y) ones(size(x));
problem_data.grad_c_diff = @(x, y) cat (1, ...
            reshape (zeros(size(x)), [1, size(x)]), ...
            reshape (zeros(size(x)), [1, size(x)]));
        
problem_data.c_reaction  = @(x, y) zeros(size(x)); 
        
% Final time
problem_data.T = 2;
        
alpha = 3; % alpha larger than 2

% Source and boundary terms
problem_data.f = @(t, x, y) rhs_singular_function_alpha(t, x, y, alpha);
problem_data.g = @(t, x, y, ind) zeros(size(x));
problem_data.h = @(t, x, y, ind) singular_function_alpha(t, x, y, alpha);

% Exact solution (optional)
problem_data.uex     = @(t, x, y) singular_function_alpha(t, x, y, alpha);
problem_data.graduex = @(t, x, y) grad_singular_function_alpha(t, x, y, alpha);
                   
problem_data.u0 = @(x,y) zeros(size(x));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


