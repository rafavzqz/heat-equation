%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PHYSICAL DATA OF THE PROBLEM
% Physical domain, defined as NURBS map given in a text file
problem_data.geo_name = 'geo_square.txt';

% Type of boundary conditions for each side of the domain
problem_data.nmnn_sides   = [];
problem_data.drchlt_sides = [1 2 3 4];

% Physical parameters
problem_data.c_diff  = @(x, y) ones(size(x));
problem_data.grad_c_diff = @(x, y) cat (1, ...
    reshape (zeros(size(x)), [1, size(x)]), ...
    reshape (zeros(size(x)), [1, size(x)]));

problem_data.c_reaction  = @(x, y) zeros(size(x));

% Final time
problem_data.T = 2;

tbar = pi/3;
alpha = 0.7;
aa = @(t,x) pi*(x.^2-x)*t;
aa_x = @(t,x) pi*(2*x-1)*t;
aa_t = @(t,x) pi*(x.^2-x);

uex = @(t,x,y) abs(t-tbar)^alpha*sin(aa(t,x)).*sin(aa(t,y));
ut = @(t,x,y)     alpha*abs(t-tbar)^(alpha-1)*sign(t-tbar)* sin(aa(t,x)).*sin(aa(t,y))...
    + abs(t-tbar)^alpha*cos(aa(t,x)).*sin(aa(t,y)).*aa_t(t,x)...
    + abs(t-tbar)^alpha*sin(aa(t,x)).*cos(aa(t,y)).*aa_t(t,y);

% Source and boundary terms
problem_data.f = @(t, x, y) ut(t,x,y)...
    + uex(t,x,y).*(aa_x(t,x).^2+aa_x(t,y).^2)...
    - 2*pi*t*abs(t-tbar)^alpha*sin(aa(t,x)+aa(t,y));
problem_data.g = @(t, x, y, ind) zeros(size(x));
problem_data.h = @(t, x, y, ind) zeros(size(x));
problem_data.u0 = @(x,y) zeros(size(x));

% Exact solution (optional)
problem_data.uex     = uex;
problem_data.graduex = @(t, x, y) cat (1, ...
    reshape (abs(t-tbar)^alpha*cos(aa(t,x)).*sin(aa(t,y)).*aa_x(t,x), [1, size(x)]), ...
    reshape (abs(t-tbar)^alpha*sin(aa(t,x)).*cos(aa(t,y)).*aa_x(t,y), [1, size(x)]));
