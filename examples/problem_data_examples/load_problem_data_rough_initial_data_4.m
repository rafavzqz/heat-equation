%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PHYSICAL DATA OF THE PROBLEM
% Physical domain, defined as NURBS map given in a text file
problem_data.geo_name = 'geo_square.txt';
problem_data.geo_name = nrb4surf ([-1 -1], [1 -1], [-1 1], [1 1]);

% Type of boundary conditions for each side of the domain
problem_data.nmnn_sides   = [1 2 3 4];
problem_data.drchlt_sides = [];

% Physical parameters
problem_data.c_diff  = @(x, y) ones(size(x));
problem_data.grad_c_diff = @(x, y) cat (1, ...
    reshape (zeros(size(x)), [1, size(x)]), ...
    reshape (zeros(size(x)), [1, size(x)]));

problem_data.c_reaction  = @(x, y) zeros(size(x));

% Final time
problem_data.T = 1;

% Source and boundary terms
problem_data.f = @(t, x, y) zeros(size(x));
problem_data.g = @(t, x, y, ind) zeros(size(x));
problem_data.h = @(t, x, y, ind) zeros(size(x));
% problem_data.u0 = @(x,y)  ( 1 - 1 * ( ( (1/2 < x & x < 1) & (0 < y & y < 1/2) ) | ...
% ( (0 < x & x < 1/2) & (1/2 < y & y < 1) ) ) )...
%      .* ( (0 < x & x < 1) & (0 < y & y < 1) );

problem_data.u0 = @(x,y)  x.*y < 0;
% problem_data.u0 = @(x,y)  ( 1 - 1 * ( ( (0 < x & x < 1) & (-1 < y & y < 0) ) | ...
% ( (-1 < x & x < 0) & (0 < y & y < 1) ) ) )...
%      .* ( (-1 < x & x < 1) & (-1 < y & y < 1) );
