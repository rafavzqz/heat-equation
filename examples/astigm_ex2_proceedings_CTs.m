%function astigm(hmsh_init, tau_init, TOL)

close all
clear taus t error_est h1_error l2_error

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PHYSICAL DATA OF THE PROBLEM 
% load_problem_data_regular
load_problem_data_moving_peak
%load_problem_data_moving_peak_simple
%load_problem_data_oscillations
%load_problem_data_singularity_in_time
%load_problem_data_rough_initial_data
% load_problem_data_rough_initial_data_4
%load_problem_data_quarter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
problem_data.T = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHOICE OF THE DISCRETIZATION PARAMETERS (Coarse mesh)
clear method_data
dim = nargin(problem_data.u0);
method_data.degree      = 2*ones(1,dim);       % Degree of the splines
method_data.regularity  = method_data.degree-1;       % Regularity of the splines
method_data.nsub_coarse = 2*ones(1,dim);       % Number of subdivisions
method_data.nsub_refine = 2*ones(1,dim);       % Number of subdivisions
method_data.nquad       = method_data.degree+1;       % Points for the Gaussian quadrature rule
method_data.space_type  = 'standard'; % The 'standard' space is not working with the 'functions' estimator
method_data.truncated   = 0;            % 0: False, 1: True
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADAPTIVITY PARAMETERS
clear adaptivity_data
adaptivity_data.C0_est = 0.25;
adaptivity_data.mark_param = .5;
adaptivity_data.mark_strategy = 'MS';
adaptivity_data.mark_param_coarsening = 0.5;
adaptivity_data.mark_param_after_coarsening = 0.1;
adaptivity_data.coarsening_flag = 'all';
adaptivity_data.max_level = 15;
adaptivity_data.max_ndof = 30000;
adaptivity_data.num_max_iter = 15;
adaptivity_data.max_nel = 30000;
adaptivity_data.flag = 'functions'; %'elements' or 'functions'
adaptivity_data.projection = 'L2';

adaptivity_data.tol = 2e-1; %2e-1; % Relative Tolerance
% Constants to split the tolerance. Default values: 1/6, 4/6, 1/6
adaptivity_data.c_TOL02 = 1e-6 / (adaptivity_data.tol * 0.475467643597177)^2 ; %1/6;
adaptivity_data.c_TOLf2 = 1e-3 / (adaptivity_data.tol * 0.475467643597177)^2; %4/6;
adaptivity_data.c_TOLH2 = 1e-1 / (adaptivity_data.tol * 0.475467643597177)^2; %1/6;
% adaptivity_data.c_TOL02 = 1/6;
% adaptivity_data.c_TOLf2 = 4/6;
% adaptivity_data.c_TOLH2 = 1/6;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plot_data.plot_solution = false;
plot_data.plot_mesh = false;
plot_data.make_video = false;
plot_data.plot_time_steps = [0 0.05 0.1];
plot_data.filename_init = 'Moving_peak';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

known_solution = isfield (problem_data, 'uex');

[U, hmsh, hspace, geometry, estimators, contador, solution_data, tolerances] = ...
    astigm_function_newCT (problem_data, method_data, adaptivity_data, plot_data);

taus = diff (solution_data.time);

save moving_peak_L2proj_newCT problem_data method_data adaptivity_data solution_data estimators contador tolerances

[U, hmsh, hspace, geometry, estimators, contador, solution_data, tolerances] = ...
    astigm_function (problem_data, method_data, adaptivity_data, plot_data);

taus = diff (solution_data.time);

save moving_peak_L2proj_oldCT problem_data method_data adaptivity_data solution_data estimators contador tolerances

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHECK A THEORETICAL RESULT
if (sum(estimators.est_consistency.^2) > tolerances.TOLf^2)
    disp('WARNING: Local tolerance in time was calculated wrongly')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SHOW THE ACHIEVED ERROR ESTIMATION
total_error_estimate = norm(estimators.Total);
fprintf('total error estimation = %f\n', total_error_estimate);
fprintf('absolute tolerance = %f\n', tolerances.absolute_tol);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% if known_solution
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     % SHOW THE ACTUAL L2(H1) ABSOLUTE ERROR
%     absolute_error = sqrt(sum(estimators.h1_error(2:end).^2.*taus));
%     fprintf('absolute error in L2(H1)-norm = %f\n', absolute_error);
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     % SHOW THE ACTUAL L2(H1) RELATIVE ERROR
%     relative_error = absolute_error/solution_data.L2_H1_norm_uex;
%     fprintf('relative error in L2(H1)-norm = %f\n', relative_error);
%     fprintf('relative tolerance = %f\n', adaptivity_data.tol);
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% end
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % PLOT LOCAL ERROR INDICATOR AT EACH TIME STEP
% figure(3)
% semilogy(solution_data.time, estimators.Total,'r.-')
% title('Total error indicator at each time step')
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % PLOT THE H1-ERROR AT EACH TIME STEP
% if (known_solution)
%     figure(4)
%     semilogy(solution_data.time, estimators.h1_error,'-*')
%     title('H1-error at each time step')
% end
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % PLOT THE TIME STEP SIZE AT EACH TIME STEP
% figure(5)
% semilogy(solution_data.time(1:end-1),taus,'r*')
% title('Time step size at each time step')
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % PLOT NUMBER OF DOFS AT EACH TIME STEP
% figure(6)
% semilogy(solution_data.time,solution_data.ndof,'bo')
% title('Number of degrees of freedom at each time step')
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % PLOT THE DISTRIBUTION OF THE USED TIME STEPS 
% figure(7)
% hist(solution_data.time,10)
% title('Number of time steps')
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % CUANTAS VECES SE ENTRA EN CADA IF EN SPACE_AND_TIME_ADAPTATION
% figure(8)
% plot(solution_data.time(1:end-1),contador.time,'r-*',solution_data.time(1:end-1),contador.space,'b-s',...
%   solution_data.time(1:end-1),contador.coarsening,'c-d',solution_data.time(1:end-1),contador.star,'k-o')
% legend('Reduce time step','Refine the mesh (spatial ind)','Refine the mesh (coarsening ind)','Refine the mesh (star ind)')
% title('How many times ... at each time step')
% 
% figure(9)
% %title('How many times ... at each time step')
% subplot(2,2,1)
% plot(solution_data.time(1:end-1),contador.time,'r-*'),
% legend('Reduce time step'),
% subplot(2,2,2)
% plot(solution_data.time(1:end-1),contador.space,'b-s'),
% legend('Refine the mesh (spatial ind)'),
% subplot(2,2,3)
% plot(solution_data.time(1:end-1),contador.coarsening,'c-d'),
% legend('Refine the mesh (coarsening ind)'),
% subplot(2,2,4)
% plot(solution_data.time(1:end-1),contador.star,'k-o'),
% legend('Refine the mesh (star ind)'),
% 
