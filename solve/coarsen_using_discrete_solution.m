function [U, hmsh, hspace, estimators_new, counter] = coarsen_using_discrete_solution ...
  (U_old, hmsh_old, hspace_old, est_space, adaptivity_data, estimators, Ltau, TOLH2, problem_data)

%%%% PREGUNTAS PARA EDUARDO
% - En la parte de coarsening, hay que recalcular consistency? Creo que no.
% - En el calculo de |||U_{n-1}|||, hay que incluir los grados de libertad del borde? (No los uso)
% - Y en el calculo del estimador de coarsening? Usan la misma norma...
% - En el calculo de |||U_{n-1}|||, se considera la parte de discretizacion en tiempo? Creo que no

counter.coarsening = 0;
counter.star = 0;

tau_old = Ltau(end-1);
tau_new = Ltau(end);

% Compute once the norm |||U_{n-1}|||
norm_U_old = compute_energy_norm (U_old, hspace_old, hmsh_old, problem_data);

% Do a first coarsening step, using the spatial indicator
% XXX WE CAN REPLACE THE ESTIMATOR BY SOMETHING DIFFERENT (gradient, energy norm...)
[marked, num_marked] = adaptivity_mark_coarsening (est_space, hmsh_old, hspace_old, adaptivity_data);

if (isempty (marked))
  coarsened = false;
else
  [hmsh, hspace] = adaptivity_coarsen (hmsh_old, hspace_old, marked, adaptivity_data);
  if (hmsh.nel == hmsh_old.nel)
    coarsened = false;
  else
    coarsened = true;
  end
end

if (~coarsened)
  hmsh = hmsh_old; hspace = hspace_old; U = U_old;
  est_coarsening = 0;
  est_interp = 0;
else
  fprintf ('COARSENING (first try): Number of elements = %d - DOFs %d \n', hmsh.nel, hspace.ndof);
  
  if (strcmpi (adaptivity_data.projection, 'Bezier'))
    U = coarsen_Bezier_projection (hspace, hmsh,  hspace_old, hmsh_old, U_old);
    if (~hspace.truncated)
      M = op_u_v_hier (hspace, hspace, hmsh);
    end
  elseif (strcmpi (adaptivity_data.projection, 'L2'))
    [U, M] = coarsen_L2_projection (hspace, hmsh, hspace_old, hmsh_old, U_old);
  else
    error ('Unknown projector for coarsening')
  end
% XXXX We have to fix this  
  if (~hspace.truncated)
    rhs = op_f_v_hier (hspace, hmsh, @(varargin) ones (size (varargin{1})));
    hspace.coeff_pou = M \ rhs;
    for ii = 1:numel(hmsh.boundary)
      hspace.boundary(ii).coeff_pou = hspace.coeff_pou(hspace.boundary(ii).dofs);
    end
  end
end

aux_tol = sqrt (estimators.est_time^2 + estimators.est_consistency^2 + tau_old*TOLH2);  


% Start a while loop, until the estimators are small enough
while (coarsened) % coarsened is only checked the first time.
  [est_coarsening, local_est_coarsening, est_interp, local_est_interp] = ...
    compute_coarsening_and_interpolation_indicators (hspace, hmsh, U, hspace_old, hmsh_old, U_old, adaptivity_data, problem_data, tau_new);

  if (est_coarsening^2 + est_interp^2 > aux_tol^2)
    counter.coarsening = counter.coarsening +1;
    refine = true;
  else
% |||U_{n-1}||| is stored in norm_U
% We now compute the norm of I^*_n(U_{n-1}) in the coarsened space.
    norm_U = compute_energy_norm (U, hspace, hmsh, problem_data);

    if (norm_U^2 - norm_U_old^2 > tau_old)
      counter.star = counter.star +1;
      refine = true;
    else
      refine = false;
    end
  end
    
  if (refine)
    nel = hmsh.nel;
    [hmsh, hspace] = mark_and_refine_coarsened (sqrt (local_est_coarsening.^2 + local_est_interp.^2), ...
      hmsh, hspace, hmsh_old, hspace_old, adaptivity_data);
    
    if (nel == hmsh.nel)
      warning ('Failure due to HB-splines with function-based estimator. Undoing coarsening')
      adaptivity_data.mark_param_after_coarsening = 0;
      adaptivity_data.flag = 'elements';
      local_est_coarsening = ones (hmsh.nel, 1); local_est_interp = 0;
    end

    if (hmsh.nel == hmsh_old.nel)
      coarsened = false;
      est_coarsening = 0;
      est_interp = 0;
      U = U_old;
    else
      if (strcmpi (adaptivity_data.projection, 'Bezier'))
        U = coarsen_Bezier_projection (hspace, hmsh,  hspace_old, hmsh_old, U_old);
        if (~hspace.truncated)
          M = op_u_v_hier (hspace, hspace, hmsh);
        end
      elseif (strcmpi (adaptivity_data.projection, 'L2'))
        [U, M] = coarsen_L2_projection (hspace, hmsh, hspace_old, hmsh_old, U_old);
      else
        error ('Unknown projector for coarsening')
      end
% XXXX We have to fix this  
      if (~hspace.truncated)
        rhs = op_f_v_hier (hspace, hmsh, @(varargin) ones (size (varargin{1})));
        hspace.coeff_pou = M \ rhs;
        for ii = 1:numel(hmsh.boundary)
          hspace.boundary(ii).coeff_pou = hspace.coeff_pou(hspace.boundary(ii).dofs);
        end
      end

    end
% XXXX I think we don't need to recompute the consistency indicator
%     [est_consistency_2, fbar] = compute_consistency_error_indicator (f, t_old, tau, hmsh);
%     est_consistency = sqrt (est_consistency_2);
  else
    break
  end

end

estimators_new.est_coarsening = est_coarsening;
estimators_new.est_interp = est_interp;
estimators_new.aux_tol = aux_tol;

end


function normU = compute_energy_norm (u, hspace, hmsh, problem_data)
%   stiff_mat = op_gradu_gradv_hier (hspace, hspace, hmsh, problem_data.c_diff)+ ...
%               op_u_v_hier (hspace, hspace, hmsh, problem_data.c_reaction);
%   hh = @(varargin) zeros(size(varargin{1}));            
%   if ~isempty(problem_data.drchlt_sides)
%     [~, dirichlet_dofs] = sp_drchlt_l2_proj (hspace, hmsh, hh, problem_data.drchlt_sides);
%     int_dofs = setdiff (1:hspace.ndof, dirichlet_dofs);
%   else
%     int_dofs = 1:hspace.ndof;
%   end
%   normU = sqrt (u(int_dofs).' * stiff_mat(int_dofs, int_dofs) * u(int_dofs));

  drchlt_dofs = sp_get_boundary_functions (hspace, hmsh, problem_data.drchlt_sides);  
  u(drchlt_dofs) = 0;

  U_nodes = hspace_eval_hmsh (u, hspace, hmsh, {'value', 'gradient'});
  gradU_nodes = U_nodes{2};
  U_nodes = U_nodes{1};

% Compute the reaction and diffusion coefficients in the quadrature points
%  of the finest mesh, to compute the coarsening indicator
  c_diff = zeros (hmsh.mesh_of_level(1).nqn, hmsh.nel);
  c_reac = c_diff;
  shifting_index = cumsum ([0 hmsh.nel_per_level]);
  for ilev = 1:hmsh.nlevels
    if (hmsh.nel_per_level(ilev) > 0)
      elements = shifting_index(ilev)+1:shifting_index(ilev+1);
      size_lev = [hmsh.msh_lev{ilev}.nqn, hmsh.msh_lev{ilev}.nel];
      for idim = 1:hmsh.rdim
        x{idim} = reshape (hmsh.msh_lev{ilev}.geo_map(idim,:,:), size_lev);
      end
      c_reac(:,elements) = problem_data.c_reaction (x{:});
      c_diff(:,elements) = problem_data.c_diff (x{:});
    end
  end

  c_diff = reshape (c_diff, [1, size(c_diff)]);
  norm_reac = sp_L2_norm (hmsh, U_nodes.*sqrt(c_reac));
  norm_diff = sp_L2_norm (hmsh, bsxfun (@times, gradU_nodes, sqrt(c_diff)));
  normU = sqrt (norm_reac^2 + norm_diff^2);

end

function [U, M] = coarsen_L2_projection (hspace, hmsh, hspace_old, hmsh_old, U_old)
  M = op_u_v_hier (hspace, hspace, hmsh);
  U = M \ op_f_v_hier_from_dofs (hspace, hmsh, hspace_old, hmsh_old, U_old);
%   G = op_u_v_hier (hspace_old, hspace_in_finer_mesh (hspace, hmsh, hmsh_old), hmsh_old);
%   U = M \ (G * U_old);
  
end

function [U, M] = coarsen_Bezier_projection (hspace, hmsh, hspace_old, hmsh_old, U_old)

% hspace:     coarsened hierarchical space
% hmsh:       coarsened hierarchical mesh
% hspace_old: original hierarchical space (finer than hspace)
% hmsh_old:   original hierarchical mesh (finer than hmsh)
% U_old:      degrees of freedom with respect to hspace_old

% I project only on the active elements

if (~hspace.truncated)
  error ('Only implemented for the truncated basis')
end

U = zeros (hspace.ndof, 1);

shifting_index = cumsum ([0 hmsh.nel_per_level]);
shifting_index_old = cumsum ([0 hmsh_old.nel_per_level]);
shifting_index_space = cumsum ([0 hspace.ndof_per_level]);

U_on_quad = hspace_eval_hmsh (U_old, hspace_old, hmsh_old);

% I can restrict M_loc, rhs_loc and u_loc to the level (active elements)

% Local mass matrices for Bernstein polynomials, and right-hand sides
for lev = 1:hmsh.nlevels
  if (hmsh.nel_per_level(lev) > 0 && hspace.ndof_per_level(lev) > 0)
    msh_lev = hmsh.msh_lev{lev};
    sp_lev = hspace.space_of_level(lev);
    [sp_bernstein, Proj] = sp_refine (sp_lev, hmsh.mesh_of_level(lev), ones(1, hmsh.ndim), sp_lev.degree, zeros(1, hmsh.ndim));
    sp_bsp_lev = sp_evaluate_element_list (sp_lev, msh_lev, 'value', false);
    sp_bern_lev = sp_evaluate_element_list (sp_bernstein, msh_lev);
    M_loc = op_u_v_local_matrices (sp_bern_lev, sp_bern_lev, msh_lev, ones(msh_lev.nqn, msh_lev.nel));

% Right-hand side on elements that did not change
    [active_elems,active_indices,active_indices_old] = intersect (hmsh.active{lev}, hmsh_old.active{lev});
    msh_loc = msh_restrict_to_cells (msh_lev, active_elems);
    sp_bern_loc = sp_evaluate_element_list (sp_bernstein, msh_loc);
    
    U_loc = U_on_quad(:,shifting_index_old(lev)+active_indices_old);
    rhs_loc(active_indices) = op_f_v_local_matrices (sp_bern_loc, msh_loc, U_loc);

% Right-hand side on coarsened elements
    [reactivated_elems,reactivated_indices] = setdiff (hmsh.active{lev}, hmsh_old.active{lev});
    if (lev ~= hmsh.nlevels)
      [removed_elems,removed_indices] = setdiff (hmsh_old.active{lev+1}, hmsh.active{lev+1});
      [~,~,children] = hmsh_get_children (hmsh_old, lev, reactivated_elems);
    elseif (hmsh.nlevels < hmsh_old.nlevels)
      removed_elems = hmsh_old.active{lev+1};
      removed_indices = 1:hmsh_old.nel_per_level(lev+1);
      [~,~,children] = hmsh_get_children (hmsh_old, lev, reactivated_elems);
    else
      removed_elems = [];
    end
    
    if (~isempty (removed_elems))
      sp_bernstein_fine = sp_bernstein.constructor (hmsh_old.mesh_of_level(lev+1));
      msh_loc = msh_restrict_to_cells (hmsh_old.msh_lev{lev+1}, removed_elems);
      sp_bern_loc = sp_evaluate_element_list (sp_bernstein_fine, msh_loc);

      U_loc = U_on_quad(:,shifting_index_old(lev+1)+removed_indices);
      rhs_loc_fine = op_f_v_local_matrices (sp_bern_loc, msh_loc, U_loc);
      [~,position] = ismember (children, removed_elems);

% Group the rhs of the children, and reshape
      rhs_loc_fine = sum (reshape (cell2mat (rhs_loc_fine(position)), [], size(children,1), size(children,2)), 2);
      rhs_loc_fine = reshape (rhs_loc_fine, [], size(children, 2));
      rhs_loc(reactivated_indices) = ...
        mat2cell (rhs_loc_fine, sp_bern_loc.nsh_max, ones(numel(reactivated_elems),1)).';
    end

% Solve local linear systems, and convert to (local) B-splines basis by Bezier extraction
    CC = Proj{1};
    for idim = 2:hmsh.ndim
      CC = kron (Proj{idim}, CC);
    end
    for iel = 1:hmsh.nel_per_level(lev)
      rows = sp_bern_lev.connectivity(:,iel);
      cols = sp_bsp_lev.connectivity(:,iel);
      CC_loc = CC(rows, cols);
      index = shifting_index(lev) + iel;
      u_loc{iel} = CC_loc \ (M_loc{iel} \ rhs_loc{iel});
    end
    
    [~,cells_per_function] = sp_get_cells (sp_lev, hmsh.mesh_of_level(lev), hspace.active{lev});
    for ifun = 1:hspace.ndof_per_level(lev)
      [~,~,elem_indices] = intersect (cells_per_function{ifun}, hmsh.active{lev});
      COEFF = 1/numel(elem_indices);
      for iel = 1:numel(elem_indices)
        cols = sp_bsp_lev.connectivity(:,elem_indices(iel));
        [~,ind_fun] = ismember (hspace.active{lev}(ifun), cols);
        U(shifting_index_space(lev)+ifun) = U(shifting_index_space(lev)+ifun) + ...
          u_loc{elem_indices(iel)}(ind_fun) * COEFF;
      end
    end
  end
end


end