function [hmsh, hspace, Cref] = mark_and_refine(est, hmsh, hspace, adaptivity_data)
%
% function [hmsh, hspace, Cref] = mark_and_refine(est, hmsh, hspace, adaptivity_data)
%   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MARK
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
disp('Marking:')
[marked, num_marked] = adaptivity_mark(est, hmsh, hspace, adaptivity_data);
tempo = toc;
switch adaptivity_data.flag
    case 'elements',
        fprintf('%d elements marked for refinement (%f seconds)\n', num_marked, tempo);
    case 'functions',
        fprintf('%d functions marked for refinement (%f seconds)\n', num_marked, tempo);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% REFINE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
disp('Refining mesh and space (spatial estimators):')
[hmsh, hspace, Cref] = adaptivity_refine (hmsh, hspace, marked, adaptivity_data);
tempo = toc;
 fprintf('DOFs = %d,\t Number of elements = %d.\t (%f seconds)\n', hspace.ndof, hmsh.nel, tempo);
