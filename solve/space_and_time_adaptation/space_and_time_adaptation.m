function [U, tau, fbar, hmsh, hspace, estimators, local_est_space, counter] = ...
   space_and_time_adaptation (U_oldc, f, t_old, tau, hmsh, hspace, problem_data, tol, adaptivity_data)
%
%
%
% Inicializamos un contador para saber cuantas veces se entra a cada if
counter.time = 0;
counter.space = 0;
% counter.coarsening = 0;
% counter.star = 0;

kappa = 0.5;

% Esta es la de coarsening
Pi_U_old = U_oldc;
[est_consistency_2, fbar] = compute_consistency_error_indicator(f, t_old, tau, hmsh);
est_consistency = sqrt(est_consistency_2);
recompute_matrices = true;
while 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % SOLVE ELLIPTIC PROBLEM
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [U_old_evals, ~] = hspace_eval_hmsh (Pi_U_old, hspace, hmsh,{'value','gradient'});
    U_old_values = U_old_evals{1};
    
    one_over_tau = 1/tau;
    elliptic_problem_data.nmnn_sides = problem_data.nmnn_sides;
    elliptic_problem_data.drchlt_sides = problem_data.drchlt_sides;
    elliptic_problem_data.c_diff = problem_data.c_diff;
    elliptic_problem_data.c_reaction = @(x,y) problem_data.c_reaction(x,y) + one_over_tau;
    elliptic_problem_data.f_at_quad_points = fbar + one_over_tau * U_old_values;
    elliptic_problem_data.g = @(x, y, ind) problem_data.g(t_old+tau, x, y, ind);
    elliptic_problem_data.h = @(x, y, ind) problem_data.h(t_old+tau, x, y, ind);

    if (recompute_matrices)
      A = op_gradu_gradv_hier (hspace, hspace, hmsh, problem_data.c_diff); + ...
          op_u_v_hier (hspace, hspace, hmsh, problem_data.c_reaction);
      M = op_u_v_hier (hspace, hspace, hmsh);
    end
    stiff_mat = A + M/tau;
    U = adaptivity_solve_elliptic_problem_new (hmsh, hspace, elliptic_problem_data, stiff_mat); % see (3)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % COMPUTE A POSTERIORI ERROR INDICATORS
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute time error indicators, and reduce the time step
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    est_time = compute_time_error_indicator(U, hmsh, hspace, tau, Pi_U_old); %grad_U_old, 
    if (est_time^2 > tol^2)
      counter.time = counter.time +1;
      tau = kappa*tau; % reduce the time step tau
      [est_consistency_2, fbar] = compute_consistency_error_indicator(f, t_old, tau, hmsh);
      est_consistency = sqrt(est_consistency_2);
      recompute_matrices = false;
      continue
    end

    aux_tol = sqrt(est_time^2 + est_consistency^2 + tau*tol);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute spatial error indicators, and refine the mesh
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    local_est_space = compute_spatial_error_indicators(U, hmsh, hspace, elliptic_problem_data, adaptivity_data, tau);
    local_est_space = sqrt(tau)*local_est_space;
%     local_est_space = tau*local_est_space;
    est_space = norm(local_est_space);
    if (est_space > aux_tol)
      counter.space = counter.space +1;
      [hmsh, hspace, Cref] = mark_and_refine (local_est_space, hmsh, hspace, adaptivity_data);
      [est_consistency_2, fbar] = compute_consistency_error_indicator (f, t_old, tau, hmsh);
      est_consistency = sqrt (est_consistency_2);

      Pi_U_old = Cref*Pi_U_old;
      recompute_matrices = true;
      continue
    end

    break
end

estimators.est_consistency = est_consistency;
estimators.est_time = est_time;
estimators.est_space = est_space;
