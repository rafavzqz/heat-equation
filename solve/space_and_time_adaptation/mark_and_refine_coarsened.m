function [hmsh, hspace] = mark_and_refine_coarsened (est, hmsh, hspace, hmsh_old, hspace_old, adaptivity_data)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MARK
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
coarsened_entities = [];
switch adaptivity_data.flag
  case 'elements'
    shift_index = cumsum ([0 hmsh.nel_per_level]);
    for ilev = 1:hmsh.nlevels
      [~,aux_ind] = intersect (hmsh.active{ilev}, hmsh_old.deactivated{ilev});
      indices = shift_index(ilev) + aux_ind;
      coarsened_entities = union (coarsened_entities, indices);
    end
  case 'functions'
    shift_index = cumsum ([0 hspace.ndof_per_level]);
    if (strcmpi (hspace.type, 'simplified'))
      for ilev = 1:hspace.nlevels
        [~,aux_ind] = intersect (hspace.active{ilev}, hspace_old.deactivated{ilev});
        indices = shift_index(ilev) + aux_ind;
        coarsened_entities = union (coarsened_entities, indices);
      end
    elseif (strcmpi (hspace.type, 'standard'))
      disp ('Be careful! This is not well implemented')
      for ilev = 1:hspace.nlevels
        [~,aux_ind] = intersect (hspace.active{ilev}, hspace_old.deactivated{ilev});
        indices = shift_index(ilev) + aux_ind;
        coarsened_entities = union (coarsened_entities, indices);
      end
    end
    
end

marked = adaptivity_mark_subset (est, hmsh, hspace, adaptivity_data, coarsened_entities);

num_marked = sum (cellfun (@numel, marked));
fprintf ('%d %s marked for refinement \n', num_marked, adaptivity_data.flag)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% REFINE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Refining mesh and space (Coarsening estimators):')
[hmsh, hspace] = adaptivity_refine (hmsh, hspace, marked, adaptivity_data);
fprintf('DOFs = %d,\t Number of elements = %d. \n', hspace.ndof, hmsh.nel);
