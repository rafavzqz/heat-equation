function [U, hmsh, hspace, geometry, estims, contador, solution_data, tolerances] = ...
  astigm_function_newCT (problem_data, method_data, adaptivity_data, plot_data)

known_solution = isfield (problem_data, 'uex');
% Initialization to zero of some variables
error_est_consistency = 0;
error_est_time = 0;
error_est_space = 0;
error_est_coarsening = 0;
error_est_interp = 0;
if (known_solution)
  h1_error = 0;
  l2_error = 0;
end

contador.time = [];
contador.space = [];
contador.coarsening = [];
contador.star = [];

if (plot_data.make_video)
  filename_video = [plot_data.filename_init, '_video'];
  video = VideoWriter (filename_video);
  video.FrameRate = 30;
  open (video);
  fig_video = figure;
  fig_video.Position = [10 10 1800 900];
  set (fig_video, 'Visible', 'off');
else
  fig_video = []; video = [];
end

if (plot_data.plot_mesh)
  fig_mesh = figure;
  fig_mesh.Position = [10 10 1800 900];
  set (fig_mesh, 'Visible', 'off');
else
  fig_mesh = [];
end
if (plot_data.plot_solution)
  fig_solution = figure;
  fig_solution.Position = [10 10 1800 900];
  set (fig_solution, 'Visible', 'off');
else
  fig_solution = [];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Definition of the hierarchical mesh and space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[hmsh, hspace, geometry] = adaptivity_initialize_laplace (problem_data, method_data);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% We estimate the L^2(H^1) norm of the exact solution in order to
%% define a tolerance for the relative error
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if known_solution
    ttt = linspace(0,problem_data.T,101);
    L2_H1_norm_uex = 0;
    for i = 1:100
        H1_norm_uex = sp_h1_error (hspace, hmsh, zeros(hspace.ndof,1), @(x,y) problem_data.uex(ttt(i),x,y), @(x,y) problem_data.graduex(ttt(i),x,y));
        L2_H1_norm_uex = L2_H1_norm_uex + H1_norm_uex^2;
    end
    L2_H1_norm_uex = L2_H1_norm_uex *(problem_data.T/100);
    L2_H1_norm_uex = sqrt(L2_H1_norm_uex);
    
    absolute_tol = adaptivity_data.tol*L2_H1_norm_uex;
else
    absolute_tol = adaptivity_data.tol;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Split the given tolerance TOL such that 
%% TOL^2 = TOL^2(0) + 4*TOL^2(f) + TOL^2(H)  . (Update later)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tol2 = absolute_tol^2;
if (~isfield (adaptivity_data, 'c_TOL02') || isempty (adaptivity_data.c_TOL02))
  TOL02 = tol2/6; TOL0 = sqrt(TOL02);
  TOLf2 = 4*tol2/6; TOLf = sqrt(TOLf2);
  TOLH2 = tol2/6; TOLH = sqrt(TOLH2);
else
  TOL02 = adaptivity_data.c_TOL02 * tol2; TOL0 = sqrt(TOL02);
  TOLf2 = adaptivity_data.c_TOLf2 * tol2; TOLf = sqrt(TOLf2);
  TOLH2 = adaptivity_data.c_TOLH2 * tol2; TOLH = sqrt(TOLH2);
end
tolerances = struct ('TOL0', TOL0, 'TOLf', TOLf, 'TOLH', TOLH, 'absolute_tol', absolute_tol);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Refine the initial mesh to approximate the initial value
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('%%%%%%%%%%%%%%%%%%%%%');
disp('Adapting the initial mesh to approximate the initial value...');
disp('%%%%%%%%%%%%%%%%%%%%%');
[U, hmsh, hspace, U_norm, info] = adapt_initial_mesh(problem_data.u0, hmsh, hspace, geometry, TOL0, adaptivity_data);
fprintf('t = 0 - Number of elements = %d - DOFs = %d\n', hmsh.nel, hspace.ndof);
disp('%%%%%%%%%%%%%%%%%%%%%');
disp('DONE')
disp('%%%%%%%%%%%%%%%%%%%%%');

nel_bc = hmsh.nel;
ndof_bc = hspace.ndof;
nel_ac = [];
ndof_ac = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute local tolerance for time discretization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('%%%%%%%%%%%%%%%%%%%%%');
disp('Computing local tolerance for time discretization...');
disp('%%%%%%%%%%%%%%%%%%%%%');
[tolf, partition_points, f_norm] = tolfind2(problem_data.f, 0, problem_data.T, TOLf, hmsh);
fprintf('TOLf = %g \n', TOLf);
fprintf('tolf = %g \n', tolf);
fprintf('Number of time steps expected = %d \n', numel(partition_points));
disp('%%%%%%%%%%%%%%%%%%%%%');
disp('DONE')
disp('%%%%%%%%%%%%%%%%%%%%%');

disp('%%%%%%%%%%%%%%%%%%%%%');
disp('Computing local tolerance for space discretization');
T = problem_data.T;
CT = 9*sqrt(T) * sqrt(f_norm^2 + U_norm^2 + T) + 2*T;
tolh = TOLH2 / CT;
tolh2 = tolh.^2;
a_paper = 0;


tolerances.tolf = tolf;
tolerances.tolh = tolh;
fprintf('tolh = %g \n', tolh);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Adaptive loop
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tau = problem_data.T; % For technical purposes. This is not an actual time step size.
n = 1;
t(n) = 0; % Initial time step

error_est = info.est_data(end);

[tau,~] = consistency (problem_data.f, t(1), tau, tolf, problem_data.T, hmsh);
U_old_coarse = U;

% Plot the mesh and solution at the initial time step (if required)
if (plot_data.plot_mesh || plot_data.plot_solution || plot_data.make_video)
  real_times = [];
  ifig = 0;
  [ifig, real_times] = plot_mesh_solution_video (plot_data, U, hmsh, hspace, geometry, fig_mesh, fig_solution, fig_video, t(n), ifig, real_times);
  plot_steps(1) = 1;
  if (plot_data.make_video)
    frame = getframe (gcf);
    writeVideo (video, frame);
  end
end

while t(n) < problem_data.T
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    fprintf('Time t_{%d} = %f + tau \n', n+1, t(n))
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    n = n + 1;

% Compute the new version of the tolerance    
% In the first iteration some computations are repeated. I don't care    
if (n > 2)
  t_nmenos2 = t(n-2);
else
  t_nmenos2 = 0;
end

[~, ~, f_norm] = tolfind2(problem_data.f, t(n-1), problem_data.T, TOLf, hmsh);
TOLH2 = TOLH2 - a_paper;
CT = 9*sqrt(T - t(n-1)) * sqrt(f_norm^2 + U_norm^2 + (T - t_nmenos2)) + 2*(T - t(n-1));
tolh = TOLH2 / CT;
tolh2 = tolh.^2;
tolerances.tolh = [tolerances.tolh tolh];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % REDUCE THE TIME STEP SIZE tau AND REFINE THE MESH hmsh
    % IN TO ORDER TO GET ERROR INDICATORS SMALL ENOUGH
    disp('%%%%%%%%%%%%%%%%%%%%%')
    disp('Space and time adaptation...')
    disp('%%%%%%%%%%%%%%%%%%%%%')
    [U, tau, fbar, hmsh, hspace, estimators, est_space, counter] = ...
      space_and_time_adaptation (U_old_coarse, problem_data.f, t(n-1), tau, hmsh, hspace, problem_data, tolh, adaptivity_data);
    contador.time = [contador.time counter.time];
    contador.space = [contador.space counter.space];
    
    [~,~,U_norm] = sp_h1_error (hspace, hmsh, U, @(x,y) zeros(size(x)), @(x,y) zeros ([2, size(x)]));
    a_paper = 3 * estimators.est_time^2 + 2 * tau *tolh;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % DEFINE THE NEW TIME STEP USING THE COMPUTED TIME STEP SIZE tau
    t(n) = t(n-1) + tau;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Error computation
    if known_solution
        err_h1 = sp_h1_error (hspace, hmsh, U, @(x,y) problem_data.uex(t(n),x,y), @(x,y) problem_data.graduex(t(n),x,y));
        h1_error = [h1_error err_h1];
        fprintf('error_H1 = %g\t at time t = %g\n', err_h1,t(n));
        err_l2 = sp_l2_error (hspace, hmsh, U, @(x,y) problem_data.uex(t(n),x,y));
        l2_error = [l2_error err_l2];
        fprintf('error_L2 = %g\t at time t = %g\n', err_l2,t(n));
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % STORE THE NECESSARY VARIABLES FOR THE NEXT TIME STEP
    tau_old = tau;
    hmsh_old = hmsh;
    hspace_old = hspace;
    U_old = U;

% Plot the mesh and solution at the current time step (if required)
    if (plot_data.plot_mesh || plot_data.plot_solution || plot_data.make_video)
      [ifig, real_times, flag] = plot_mesh_solution_video (plot_data, U, hmsh, hspace, geometry, fig_mesh, fig_solution, fig_video, t(n), ifig, real_times);
      if (flag); plot_steps(ifig) = n; end
      if (plot_data.make_video)
        frame = getframe (gcf);
        writeVideo (video, frame);
      end
    end

% Condition here to check whether we are in the final step,
%  and avoid doing useless computations (consistency and coarsening)
    if (t(n) < problem_data.T)
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % ADAPT (ENLARGE OR REDUCE) THE TIME STEP SIZE tau(n) 
      % UNTIL THE CONSISTENCY ERROR INDICATOR IS LOWER THAN tolf
      tau = min (tau, problem_data.T-t(n));
      [tau,~] = consistency (problem_data.f, t(n), tau, tolf, problem_data.T, hmsh);
      tau = min (2*tau_old, tau); % Be more conservative in coarsening the time step size

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % COARSEN THE HIERARCHICAL MESH hmsh_old.
      disp('%%%%%%%%%%%%%%%%%%%%%')
      disp('Coarsen the mesh and space...')
      disp('%%%%%%%%%%%%%%%%%%%%%')
      [U_old_coarse, hmsh, hspace, estimators2, counter] = ...
          coarsen_using_discrete_solution (U_old, hmsh_old, hspace_old, est_space, adaptivity_data, estimators, [tau_old tau], tolh2, problem_data);
      fprintf('(BEFORE COARSENING) Number of elements = %d - DOFs = %d\n', hmsh_old.nel, hspace_old.ndof);
      fprintf('(AFTER  COARSENING) Number of elements = %d - DOFs = %d\n', hmsh.nel, hspace.ndof);
      contador.coarsening= [contador.coarsening counter.coarsening];
      contador.star = [contador.star counter.star];
    else
      estimators2.est_coarsening = 0; estimators2.interp = 0;
      contador.coarsening = [contador.coarsening 0]; contador.star = [contador.star 0];
    end
    nel_bc =  [nel_bc hmsh_old.nel];
    ndof_bc = [ndof_bc hspace_old.ndof];
    nel_ac =  [nel_ac hmsh.nel];
    ndof_ac = [ndof_ac hspace.ndof];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % SHOW AND STORE INFORMATION ABOUT ERROR INDICATORS
    fprintf('t = %f - Number of elements = %d - DOFs = %d\n', t(n), hmsh.nel, hspace.ndof);
    fprintf(' est_consistency = %f,\n est_time = %f,\n est_space = %f,\n est_coarsening = %f.\n est_interp = %f.\n',...
        estimators.est_consistency, estimators.est_time, estimators.est_space, estimators2.est_coarsening, estimators2.est_interp);
    est = sqrt(estimators.est_consistency^2 + estimators.est_time^2 + estimators.est_space^2 + estimators2.est_coarsening^2 + estimators2.est_interp^2);
    fprintf('est = %f\n', est);
    error_est = [error_est est];
    error_est_consistency = [error_est_consistency estimators.est_consistency];
    error_est_time = [error_est_time estimators.est_time];
    error_est_space = [error_est_space estimators.est_space];
    error_est_coarsening = [error_est_coarsening estimators2.est_coarsening];
    error_est_interp = [error_est_interp estimators2.est_interp];

end % End Adaptive loop

if (plot_data.make_video), close (video), end

% Store the estimators in one single struct
estims = struct ('Total', error_est, 'est_consistency', error_est_consistency, 'est_time', error_est_time, ...
           'est_space', error_est_space, 'est_coarsening', error_est_coarsening, 'est_interp', error_est_interp);
solution_data = struct ('nel', nel_bc, 'ndof', ndof_bc, 'time', t, 'nel_ac', nel_ac, 'ndof_ac', ndof_ac);

if (known_solution)
  estims.h1_error = h1_error; estims.l2_error = l2_error;
  solution_data.H1_norm_uex = H1_norm_uex;
  solution_data.L2_H1_norm_uex = L2_H1_norm_uex;
end
if (plot_data.plot_solution || plot_data.plot_mesh)
  solution_data.plot_real_times = real_times;
  solution_data.plot_steps = plot_steps;
end

end