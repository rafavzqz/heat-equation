% 
% 
%

function [E2, fbar, norm_f2] = compute_fbar_and_error (f, interval, quad_points, w)
% global clt_qpx1 clt_qpx2 clt_qpw; % quadrature points and weights for consistency


qpt = [-sqrt(3/5) 0 sqrt(3/5)];
qpW = 1/9*[5 8 5]'; qpW = qpW/2;
a = interval(1);
b = interval(2);
len = b-a;

t = (a+b)/2+qpt*(len/2);

fbar = zeros(size(w));
intf2 = zeros(size(w));

% I need these to compute the norm of f in L^2(\Omega x (0,T))
% I may be repeating computations, but this was safer
qw = zeros ([3, size(w)]);
ft2_on_pts = zeros ([3, size(w)]);
for i = 1:length(t)
  ft = squeeze(feval(f, t(i), quad_points(1,:,:), quad_points(2,:,:)));
  fbar = fbar + ft*qpW(i);
  intf2 = intf2 + ft.^2*qpW(i);
  qw(i,:,:) = qpW(i) * w;
  
  ft2_on_pts(i,:,:) = ft.^2;
end
intf2 = len*intf2;
    
%fbar = ft*qpW;
%intf2 = ft.^2*(qpW*len);

e2 = intf2 - len*fbar.^2;
E2 = sum(sum(e2.*w));

% % for debugging
% tt = linspace(a,b,30);
% ftt = feval(f,tt);
% plot(tt,ftt,[a,b],[fbar,fbar])

norm_f2 = sum(ft2_on_pts(:).*qw(:));
end