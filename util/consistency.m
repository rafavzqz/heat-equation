function [adapted_tau, fbar] = consistency(f, t , tau , tol, T, hmsh)
%
% function [adapted_tau, fbar] = consistency(f, t , tau , tol, T, hmsh)
%

sigma = 0.9;
k1 = 0.9;
k2 = 1.1;
tol2 = tol^2;

[est_consistency_2, fbar] = compute_consistency_error_indicator(f, t, tau, hmsh);

while (est_consistency_2 < sigma*tol2 && tau < (T-t))
    tau = min(k2*tau, T-t);
    [est_consistency_2, fbar] = compute_consistency_error_indicator(f, t, tau, hmsh);
end

while est_consistency_2 > tol2
    tau = k1*tau;
    [est_consistency_2, fbar] = compute_consistency_error_indicator(f, t, tau, hmsh);
end

adapted_tau = tau;