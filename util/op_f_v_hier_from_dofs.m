% OP_F_V_HIER: assemble the right-hand side vector r = [r(i)], with  r(i) = (f, v_i) for hierarchical splines, 
%  exploiting the multilevel structure.
%
%   rhs = op_f_v_hier (hspace, hmsh, coeff);
%
% INPUT:
%     
%   hspace: object representing the hierarchical space of test functions (see hierarchical_space)
%   hmsh:   object representing the hierarchical mesh (see hierarchical_mesh)
%   coeff:  function handle to compute the source function
%
% OUTPUT:
%
%   rhs: assembled right-hand side
% 
% The multilevel structure is exploited in such a way that only functions
%  of the same level of the active elements have to be computed. See also
%  op_gradu_gradv_hier for more details.
%
% Copyright (C) 2015 Eduardo M. Garau, Rafael Vazquez
%
%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.

function rhs = op_f_v_hier_from_dofs (hspace, hmsh, hspace_fine, hmsh_fine, u_fine)

  hspace = hspace_in_finer_mesh (hspace, hmsh, hmsh_fine);
  
  valu = hspace_eval_hmsh (u_fine, hspace_fine, hmsh_fine);

  rhs = zeros (hspace.ndof, 1);

  ndofs = 0;
  shifting_index = cumsum ([0 hmsh_fine.nel_per_level]);
  for ilev = 1:hmsh_fine.nlevels
    ndofs = ndofs + hspace.ndof_per_level(ilev);
    if (hmsh_fine.nel_per_level(ilev) > 0)
%       x = cell (hmsh_fine.rdim, 1);
%       for idim = 1:hmsh_fine.rdim
%         x{idim} = reshape (hmsh_fine.msh_lev{ilev}.geo_map(idim,:,:), hmsh_fine.mesh_of_level(ilev).nqn, hmsh_fine.nel_per_level(ilev));
%       end
      sp_lev = sp_evaluate_element_list (hspace.space_of_level(ilev), hmsh_fine.msh_lev{ilev}, 'value', true);
      
      sp_lev = change_connectivity_localized_Csub (sp_lev, hspace, ilev);
      
      u_on_quad = valu(:,shifting_index(ilev)+1:shifting_index(ilev+1));
      b_lev = op_f_v (sp_lev, hmsh_fine.msh_lev{ilev}, u_on_quad);

      dofs = 1:ndofs;
      rhs(dofs) = rhs(dofs) + hspace.Csub{ilev}.' * b_lev;
    end
  end

end
