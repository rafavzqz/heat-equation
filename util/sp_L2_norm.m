% SP_L2_NORM: Evaluate the L2-norm.
%
%   [L2_norm, L2_norm_elem] = sp_L2_norm (hmsh, U);
%
% INPUT:
%
%     hmsh:    
%     U:        vector of dof weights
%
% OUTPUT:
%


function [L2_norm, L2_norm_elem] = sp_L2_norm (hmsh, U)

nqn = hmsh.mesh_of_level(1).nqn;

n = numel(size(U));

if n == 3
    U = reshape (U, hmsh.rdim, nqn, hmsh.nel);
end

w = [];
for ilev = 1:hmsh.nlevels
    if (hmsh.msh_lev{ilev}.nel ~= 0)
        w = cat (2, w, hmsh.msh_lev{ilev}.quad_weights .* hmsh.msh_lev{ilev}.jacdet);
    end
end

switch n,
    case 2, L2_norm_elem = sqrt(sum(U.^2.* w));
    case 3, L2_norm_elem = sqrt(sum(reshape(sum(U.^2, 1), [nqn, hmsh.nel]).* w));
end

L2_norm = norm(L2_norm_elem);

