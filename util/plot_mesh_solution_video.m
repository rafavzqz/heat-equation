function [fig_number, real_times, figure_plots] = ...
  plot_mesh_solution_video (plot_data, U, hmsh, hspace, geometry, fig_mesh, fig_solution, fig_video, t, fig_number, real_times)

figure_plots = false;
if (plot_data.plot_mesh || plot_data.plot_solution)
  if (fig_number+1 > numel(plot_data.plot_time_steps))
    disp('All plots done')
  elseif (t >= plot_data.plot_time_steps(fig_number+1))
    real_times(fig_number+1) = t;
    fig_number = fig_number + 1;
    figure_plots = true;
  else
    real_times = real_times;
  end
end

% For now I use a fixed value of npts and npts_mesh. If we want to change it, 
%  we should add them to plot_data
if (plot_data.plot_mesh && figure_plots)
  npts_mesh = 2;
  hmsh_plot_cells (hmsh, npts_mesh, fig_mesh); view(2), axis off, axis square
  title(sprintf('t = %f - Number of elements = %d - DOFs = %d', t, hmsh.nel, hspace.ndof))
  set (gca, 'FontSize', 16)
  filename_mesh = sprintf ('%s_mesh_%i', plot_data.filename_init, fig_number);
  print (filename_mesh, '-deps')
end

if (plot_data.plot_solution && figure_plots)
  set (groot, 'CurrentFigure', fig_solution);
  npts = 51 * ones (1, hmsh.ndim);
  sp_plot_solution (U, hspace, geometry, npts);
  shading interp
  title (sprintf ('Numerical solution at time t = %f', t))
  set (gca, 'FontSize', 16)
  filename_solution = sprintf ('%s_solution_%i', plot_data.filename_init, fig_number);
  print (filename_solution, '-depsc')
end

if (plot_data.make_video)
  set (groot, 'CurrentFigure', fig_video);
  hh1 = subplot (1,2,1);
  npts_mesh = 2;
  hmsh_plot_cells (hmsh, npts_mesh, fig_video); view(2), axis off, axis square
  title(sprintf('t = %f - Number of elements = %d - DOFs = %d', t, hmsh.nel, hspace.ndof))

  hh2 = subplot (1, 2, 2);
  npts = 51 * ones (1, hmsh.ndim);
  sp_plot_solution (U, hspace, geometry, npts);
  shading interp
  title ('Numerical solution')
  set (gca, 'FontSize', 16)
  
  zlim ([-1.05 1.05]); caxis ([0 1]);
end

end
