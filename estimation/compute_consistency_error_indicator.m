function [est_consistency_2, fbar] = compute_consistency_error_indicator(f, t, tau, hmsh)
%
% function [est_consistency_2, fbar] = compute_consistency_error_indicator(f, t, tau, hmsh)
%


w = [];
quad_points = [];
for ilev = 1:hmsh.nlevels
    if (hmsh.msh_lev{ilev}.nel ~= 0)
        w = cat (2, w, hmsh.msh_lev{ilev}.quad_weights .* hmsh.msh_lev{ilev}.jacdet);
        quad_points = cat (3, quad_points, hmsh.msh_lev{ilev}.geo_map);
    end
end

% qp = cell(hmsh.rdim,1);
% 
% for i = 1:hmsh.rdim
%     qp{i} = quad_points(i,:,:);
%     qp{i} = qp{i}(:);
% end
% w = w(:);

[est_consistency_2, fbar] = compute_fbar_and_error(f, [t t+tau], quad_points, w);