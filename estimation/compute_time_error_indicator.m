function est = compute_time_error_indicator (U, hmsh, hspace, tau, U_old) %, grad_U_old)

zerof = @(varargin) zeros (size(varargin{1}));
zerodf = @(varargin) zeros ([hmsh.rdim, size(varargin{1})]);

[~,~,err_h1s] = sp_h1_error (hspace, hmsh, U-U_old, zerof, zerodf);
est = sqrt(2 * tau) * err_h1s;

% % Grad_U_old es la proyeccion en la malla n
% 
% [grad_U, ~] = hspace_eval_hmsh (U, hspace, hmsh, 'gradient');
% 
% est = sqrt(tau)*sp_L2_norm(hmsh, grad_U_old-grad_U);
