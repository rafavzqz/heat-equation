function [est_coarsen, est_coarsen_local, est_interp, est_interp_local] = ...
  compute_coarsening_and_interpolation_indicators (hspace, hmsh, U, hspace_old, hmsh_old, U_old, adaptivity_data, problem_data, tau)

% Write both solutions in the finest mesh (hmsh_old), to compute integrals.
% We remove the contribution of boundary functions.
  drchlt_dofs = sp_get_boundary_functions (hspace, hmsh, problem_data.drchlt_sides);  
  drchlt_dofs_old = sp_get_boundary_functions (hspace_old, hmsh_old, problem_data.drchlt_sides);  
  U(drchlt_dofs) = 0;
  U(drchlt_dofs_old) = 0;

  hspace_fine_mesh = hspace_in_finer_mesh (hspace, hmsh, hmsh_old);
  U_nodes = hspace_eval_hmsh (U, hspace_fine_mesh, hmsh_old, {'value', 'gradient'});
  U_old_nodes = hspace_eval_hmsh (U_old, hspace_old, hmsh_old, {'value', 'gradient'});
  U_diff = U_nodes{1} - U_old_nodes{1};
  gradU_diff = U_nodes{2} - U_old_nodes{2};
  clear U_nodes U_old_nodes

% Compute the reaction and diffusion coefficients in the quadrature points
%  of the finest mesh, to compute the coarsening indicator
  c_diff = zeros (hmsh_old.mesh_of_level(1).nqn, hmsh_old.nel);
  c_reac = c_diff;
  shifting_index_old = cumsum ([0 hmsh_old.nel_per_level]);
  for ilev = 1:hmsh_old.nlevels
    if (hmsh_old.nel_per_level(ilev) > 0)
      elements_old = shifting_index_old(ilev)+1:shifting_index_old(ilev+1);
      size_lev = [hmsh_old.msh_lev{ilev}.nqn, hmsh_old.msh_lev{ilev}.nel];
      for idim = 1:hmsh.rdim
        x{idim} = reshape (hmsh_old.msh_lev{ilev}.geo_map(idim,:,:), size_lev);
      end
      c_reac(:,elements_old) = problem_data.c_reaction (x{:});
      c_diff(:,elements_old) = problem_data.c_diff (x{:});
    end
  end

  
% COARSENING AND INTERPOLATION INDICATORS. 
% For 'elements' the indicators are first computed in the fine mesh, and
% are translated to the fine coarse mesh below.

  switch (adaptivity_data.flag)
   case ('elements')
    c_diff = reshape (c_diff, [1, size(c_diff)]);
    [est_coarsen_reac, est_reac_old_mesh] = sp_L2_norm (hmsh_old, U_diff.*sqrt(c_reac));
    [est_coarsen_diff, est_diff_old_mesh] = sp_L2_norm (hmsh_old, bsxfun (@times, gradU_diff, sqrt(c_diff)));
    [est_interp, est_interp_old_mesh] = sp_L2_norm (hmsh_old, U_diff);
    est_coarsen = sqrt (2 * tau * (est_coarsen_reac.^2 + est_coarsen_diff.^2));
    est_coarsen_old_mesh = sqrt (2 * tau * (est_reac_old_mesh.^2 + est_diff_old_mesh.^2));
   case ('functions')
    shifting_index = cumsum ([0 hmsh_old.nel_per_level]);
    ndofs = 0;
    est_coarsen_local = zeros (hspace.ndof, 1);
    est_interp_local = zeros (hspace.ndof, 1);
    for ilev = 1:hmsh_old.nlevels
      ndofs = ndofs + hspace_fine_mesh.ndof_per_level(ilev);
      if (hmsh_old.nel_per_level(ilev) > 0)
        size_lev = [hmsh_old.msh_lev{ilev}.nqn, hmsh_old.msh_lev{ilev}.nel];
        elements = shifting_index(ilev)+1:shifting_index(ilev+1);

        Udiff_at_lev = U_diff(:,elements).^2;
        gradUdiff_at_lev = reshape (sum (gradU_diff(:,:,elements).^2, 1), size_lev);

        sp_lev = sp_evaluate_element_list (hspace_fine_mesh.space_of_level(ilev), hmsh_old.msh_lev{ilev}, 'value', true, 'gradient', true);
        sp_lev = change_connectivity_localized_Csub (sp_lev, hspace_fine_mesh, ilev);
        b_lev = op_f_v (sp_lev, hmsh_old.msh_lev{ilev}, Udiff_at_lev);
        breac_lev = op_f_v (sp_lev, hmsh_old.msh_lev{ilev}, Udiff_at_lev.*c_reac(:,elements));
        bdiff_lev = op_f_v (sp_lev, hmsh_old.msh_lev{ilev}, gradUdiff_at_lev.*c_diff(:,elements));

        dofs = 1:ndofs;
        est_interp_local(dofs) = est_interp_local(dofs) + hspace_fine_mesh.Csub{ilev}.' * b_lev;
        est_coarsen_local(dofs) = est_coarsen_local(dofs) + hspace_fine_mesh.Csub{ilev}.' * (breac_lev + bdiff_lev);
      end
    end
    est_interp_local = sqrt (est_interp_local .* hspace.coeff_pou(:));
    est_interp = sqrt (sum (est_interp_local.^2));
    est_coarsen_local = sqrt (2 * tau * est_coarsen_local .* hspace.coeff_pou(:));
    est_coarsen = sqrt (sum (est_coarsen_local.^2));
  end


% Pass the estimators from the fine to the coarse mesh
  if (strcmpi (adaptivity_data.flag, 'elements'))
    if (hmsh.nel ~= hmsh_old.nel)
      est_interp_local = zeros (hmsh.nel, 1);
      est_coarsen_local = zeros (hmsh.nel, 1);
      shifting_index = cumsum ([0 hmsh.nel_per_level]);
      shifting_index_old = cumsum ([0 hmsh_old.nel_per_level]);
      for ilev = 1:hmsh.nlevels
        if (hmsh.nel_per_level(ilev) > 0)
          elements = shifting_index(ilev)+1:shifting_index(ilev+1);
          elements_old = shifting_index_old(ilev)+1:shifting_index_old(ilev+1);
          [active_in_both, ind, ind_old] = intersect (hmsh.active{ilev}, hmsh_old.active{ilev});
          [active_in_coarse, ind_active] = setdiff (hmsh.active{ilev}, active_in_both);
          est_interp_local(elements(ind)) = est_interp_old_mesh(elements_old(ind_old));
          est_coarsen_local(elements(ind)) = est_coarsen_old_mesh(elements_old(ind_old));
          if (numel (active_in_coarse) > 0)
            elements_old = shifting_index_old(ilev+1)+1:shifting_index_old(ilev+2);
            [~, ~, children_per_cell] = hmsh_get_children (hmsh_old, ilev, active_in_coarse);
            [~,~,ind_old] = intersect (children_per_cell, hmsh_old.active{ilev+1}, 'stable');
            elem_old = reshape (elements_old(ind_old), size(children_per_cell));
            est_interp_old = reshape (est_interp_old_mesh(elem_old), size(children_per_cell));
            est_coarsen_old = reshape (est_coarsen_old_mesh(elem_old), size(children_per_cell));
            est_interp_local(elements(ind_active)) = sqrt (sum (est_interp_old.^2, 1));
            est_coarsen_local(elements(ind_active)) = sqrt (sum (est_coarsen_old.^2, 1));
          end
        end
      end
    else
      est_interp_local = est_interp_old_mesh;
      est_coarsen_local = est_coarsen_old_mesh;
    end
  end

end